﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Basics.CustomPolicyProvider.CustomAuthorizationPolicyProvider;

namespace Basics.CustomPolicyProvider
{
    public class SecirutyLevelAttribute : AuthorizeAttribute
    {
        //Create SecurityLevel for controller Authen [SecirutyLevel(5)]
        public SecirutyLevelAttribute(int level)
        {
            Policy = $"{DynamicPolicies.SecurityLevel}.{level}";
        }
    }

    public class CustomAuthorizationPolicyProvider : DefaultAuthorizationPolicyProvider
    {
        //{Type} static policy
        public static class DynamicPolicies
        {
            public static IEnumerable<string> Get()
            {
                yield return SecurityLevel;
                yield return Rank;
            }

            public const string SecurityLevel = "SecurityLevel";
            public const string Rank = "Rank";
        }

        //tách policy tách và lấy policy
        public static class DynamicAuthorizationPolicyFactory { 
            public static AuthorizationPolicy Create (string policyName)
            {
                var parts = policyName.Split(".");
                var type = parts.First();
                var value = parts.Last();

                switch (type)
                {
                    case DynamicPolicies.Rank:
                        return new AuthorizationPolicyBuilder().RequireClaim("Rank", value).Build();
                    case DynamicPolicies.SecurityLevel:
                        return new AuthorizationPolicyBuilder().AddRequirements(new SecurityLevelRequirement(Convert.ToInt32(value))).Build();
                    default:
                        return null;
                }
            }
        }

        //the end handle allow access
        public class SecurityLevelHandler : AuthorizationHandler<SecurityLevelRequirement>
        {
            protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, SecurityLevelRequirement requirement)
            {
                var claimValue = Convert.ToInt32(context.User.Claims.FirstOrDefault(x=>x.Type == DynamicPolicies.SecurityLevel)?.Value ?? "0");

                if (requirement.Level <= claimValue)
                {
                    context.Succeed(requirement);
                }
                return Task.CompletedTask;
            }
        }

        //level
        public class SecurityLevelRequirement : IAuthorizationRequirement
        {
            public int Level { get; }
            public SecurityLevelRequirement(int level)
            {
                Level = level;
            }
        }

        public CustomAuthorizationPolicyProvider(IOptions<AuthorizationOptions> options) : base(options)
        {

        }

        //{Type}.{Value}
        //Get policy lấy danh sách policy
        public override Task<AuthorizationPolicy> GetPolicyAsync(string policyName)
        {

            foreach (var customPolicy in DynamicPolicies.Get())
            {
                if (policyName.StartsWith(customPolicy))
                {
                    var policy = DynamicAuthorizationPolicyFactory.Create(policyName);

                    return Task.FromResult(policy);
                }
            }

            return base.GetPolicyAsync(policyName);
        }

    }
}
